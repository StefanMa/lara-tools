import io
import os
import re
import setuptools


def read(filename):
    filename = os.path.join(os.path.dirname(__file__), filename)
    text_type = type(u"")
    with io.open(filename, mode="r", encoding='utf-8') as fd:
        return re.sub(
            text_type(r':[a-z]+:`~?(.*?)`'),
            text_type(r'``\1``'),
            fd.read()
        )


setuptools.setup(
    name="lara_simulation",
    version="0.0.1",
    url="https://gitlab.com/SiLA2/sila_python/-/tree/master/sila_tools/sila2_simulation",
    license='MIT',

    author="Stefan Maak",
    author_email="maaks@uni-greifswald.de",

    description="A toolbox to create and run device simulations including an example for the greifswald laboratory.",
    long_description=read("README.rst"),
    long_description_content_type='test/x-rst',
    packages=setuptools.find_packages(exclude=('tests')),
    install_requires=['click', 'pyserial'],
    entry_points={
        'console_scripts': [
            'some-magic='
            'lara_simulation.cli:cli_group'
        ],
    },
)
