import time
import traceback
from typing import Dict
from sila2.server import SilaServer
from sila2.client import SilaClient
from dash import dcc, html, no_update
import logging
from dash.dependencies import State
from dash_extensions.enrich import Input, Output, MultiplexerTransform, DashProxy
import dash_interactive_graphviz
from threading import Thread
import threading
from lara_simulation.zookeeper import (
    ports_by_name, server_type, parse_args, server_args, interacting, setup_interaction, ZooKeeper, lara_names,
    cert_file_name
)
from lara_simulation.logging_manager import configure_logging
import plotly.graph_objects as go
from PIL import Image
import os

labels = dict(
    arm='Fanuc F5 Robotic Arm',
    centrifuge='Hettich Rotanta 460R',
    hotel='Plate Storage Carousel',
    cytomat='Thermo Cytomat2 (C1)',
    cytomat2='Thermo Cytomat2 (C2)',
    reader='Thermo VarioskanLUX (lower)',
    reader2='Thermo VarioskanLUX (upper)',
    bc_reader='Omron MS3 BarcodeReader',
    bravo='Agilent Bravo',
    cytomat3='Thermo Cytomat2 (C470)',
    cytomat4='Thermo Cytomat2 (2C)',
)


class ZooDashApp:
    app: DashProxy
    p: Thread
    servers: Dict[str, SilaServer]

    def __init__(self, simulation: bool, ip = '0.0.0.0'):
        self.ip = ip
        self.simulation = simulation
        self.servers = {}
        self.app = DashProxy(
            "Zookeeper_webinterface", prevent_initial_callbacks=True, transforms=[MultiplexerTransform()])
        self.p = Thread(target=self.app.run_server, daemon=True, kwargs=dict(debug=False, port=8051))
        pic = go.Figure()
        img_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'zookeeper.png')
        pic.add_layout_image(source=Image.open(img_path))
        self.app.layout = html.Div(children=[
            html.Div(children=''' A web application to start, restart and stop the SiLA2 servers on the LARA robotic
                     platform.'''
                     ),
            html.Button("Start Servers", id='start', n_clicks=0),
            html.Button("Stop Servers", id='stop', n_clicks=0),
            html.Button("Restart Servers", id='restart', n_clicks=0),
            html.Button("Reset Simulation", id='reset', n_clicks=0),
            dcc.Checklist(id='secure', options=[{'label': 'Secure?', 'value': 'yes'}], value=[]),
            dcc.Checklist(id="device_list",
                          options=[dict(label=label, value=key) for key, label in
                                   labels.items()],
                          value=['arm', 'cytomat2'],
                          style={"display": 'vertical'},
                          labelStyle={'display': 'block', 'color': 'red'},
                          ),
            dcc.Interval(
                id='interval-component',
                interval=1000,
                n_intervals=0
            ),
            dcc.Graph(figure=self.create_picture(img_path)),
        ])

        @self.app.callback(
            Output(component_id='device_list', component_property='value'),  # no idea why this is needed
            Input(component_id='start', component_property='n_clicks'),
            State(component_id='device_list', component_property='value'),
            State(component_id='secure', component_property='value'),
        )
        def start_all(buff, chosen_devices, secure):
            for name in chosen_devices:
                if name not in self.servers:
                    print(f"starting:{name}")
                    server = server_type[name](simulation=self.simulation, **server_args[name])
                    assert isinstance(server, SilaServer)
                    if secure:
                        print("secure connection")
                        server.start(self.ip, ports_by_name[name])
                        with open(cert_file_name(lara_names[name]), "wb") as fp:
                            fp.write(server.generated_ca)
                    else:
                        server.start_insecure(self.ip, ports_by_name[name], enable_discovery=True)
                    self.servers[name] = server
                    if name == "arm":
                        print("adding all existing devices to arms robot-interaction")
                        setup_interaction(
                            interaction_devices=[name for name in self.servers if name in interacting],
                            connect_barcode_reader="bc_reader" in self.servers
                        )
                    else:
                        if "arm" in self.servers:
                            if name in interacting:
                                print(f"adding {name} to robot_interaction")
                                setup_interaction(interaction_devices=[name], connect_barcode_reader=False, ip=ip)
                            elif name == "bc_reader":
                                print("connecting arm to barcode reader")
                                setup_interaction(interaction_devices=[], connect_barcode_reader=True, ip=ip)
            return []

        @self.app.callback(
            Output(component_id='device_list', component_property='value'),  # no idea why this is needed
            Input(component_id='stop', component_property='n_clicks'),
            State(component_id='device_list', component_property='value'),
        )
        def stop_all(n_clicks, value):
            for name in value:
                if name in self.servers:
                    print(f"stopping:{name}")
                    server = self.servers.pop(name)
                    try:
                        server.stop()
                    except Exception as ex:
                        logging.warning(f"Stopping server of {name} failed: {ex}, {traceback.print_exc()}")
            return []

        @self.app.callback(
            Output(component_id='device_list', component_property='value'),  # no idea why this is needed
            Input(component_id='reset', component_property='n_clicks'),
            State(component_id='device_list', component_property='value')
        )
        def reset_all(n_clicks, value):
            virtual_device_dict = {name : self.servers[name].hardware_interface for name in value}
            for name in virtual_device_dict:
                # clear file of inner state
                filename = f'{lara_names[name]}_inner_status_sim.json'
                print(filename)
                if filename in os.listdir():
                    os.remove(filename)
            ZooKeeper.reset(virtual_device_dict)
            return no_update

        @self.app.callback(
            Output(component_id='device_list', component_property='value'),  # no idea why this is needed
            Input(component_id='restart', component_property='n_clicks'),
            State(component_id='device_list', component_property='value'),
            State(component_id='secure', component_property='value'),
        )
        def restart_all(n_clicks, value, secure):
            for name in value:
                if name in self.servers:
                    print(f"restarting:{name}")
                    server = self.servers.pop(name)
                    try:
                        server.stop()
                    except Exception as ex:
                        logging.warning(f"Stopping server of {name} failed: {ex}, {traceback.print_exc()}")
                    server = server_type[name](simulation=self.simulation, **server_args[name])
                    if secure:
                        print("secure connection")
                        server.start(self.ip, ports_by_name[name])
                        with open(cert_file_name(lara_names[name]), "wb") as fp:
                            fp.write(server.generated_ca)
                    else:
                        server.start_insecure(self.ip, ports_by_name[name], enable_discovery=True)
                    self.servers[name] = server
            return []

        @self.app.callback(
            State(component_id='device_list', component_property='options'),
            Input(component_id='interval-component', component_property='n_intervals'),
            Output(component_id='device_list', component_property='options'),
        )
        def update_whats_running(options, n_intervals):
            virtual_device_dict = {name : server.hardware_interface for name, server in self.servers.items()}
            ZooKeeper.print_stati(virtual_device_dict)
            for option in options:
                name = option['value']
                option['label'] = labels[name]
                # server_name = SilaClient('127.0.0.1', ports_by_name[name], insecure=True).SiLAService.ServerName.get()
                # logging.debug(f"{server_name} is up :-)")
                if name in self.servers:
                    option['label'] += " (running)"
            return options

    def run(self):
        if self.p.is_alive():
            logging.warning('Server is already running. Restarting server')
            self.stop()
        logging.getLogger('werkzeug').setLevel(logging.ERROR)
        self.p.start()

    def stop(self):
        print("Sorry, I don't know, how to stop")

    def create_picture(self, filepath: str):
        # Create figure
        fig = go.Figure()

        # Constants
        img_width = 1000
        img_height = 900
        scale_factor = 0.5

        # Add invisible scatter trace.
        # This trace is added to help the autoresize logic work.
        fig.add_trace(
            go.Scatter(
                x=[0, img_width * scale_factor],
                y=[0, img_height * scale_factor],
                mode="markers",
            )
        )

        # Configure axes
        fig.update_xaxes(
            visible=False,
            range=[0, img_width * scale_factor]
        )

        fig.update_yaxes(
            visible=False,
            range=[0, img_height * scale_factor],
            # the scaleanchor attribute ensures that the aspect ratio stays constant
            scaleanchor="x"
        )

        # Add image
        fig.add_layout_image(
            dict(
                x=0,
                sizex=img_width * scale_factor,
                y=img_height * scale_factor,
                sizey=img_height * scale_factor,
                xref="x",
                yref="y",
                opacity=1.0,
                layer="below",
                sizing="stretch",
                source=Image.open(filepath))
        )

        # Configure other layout
        fig.update_layout(
            width=img_width * scale_factor,
            height=img_height * scale_factor,
            margin={"l": 0, "r": 0, "t": 0, "b": 0},
        )
        return fig


if __name__ == '__main__':
    args = parse_args()
    configure_logging(stream_lvl=logging.WARNING)
    #logging.getLogger().setLevel(logging.DEBUG)
    ip = "0.0.0.0"
    #if args.lara:
    #    ip = '141.53.254.27'
    app = ZooDashApp(simulation=not args.lara, ip=ip)
    app.run()
    while True:
        time.sleep(1)
