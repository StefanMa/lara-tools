from threading import Timer, Thread
from time import sleep
import re
import logging
from abc import ABC, abstractmethod


class SimulatedDevice(ABC):
    fast_mode: bool
    def __init__(self):
        self.fast_mode = True

    def get_in_fast_mode(self):
        self.fast_mode = True

    def get_in_real_mode(self):
        self.fast_mode = False

    def fatal_crash(self, msg=''):
        logging.error(f"AAAAAHHHHHHHRGGGGGG!!!!!!  {msg}")

    def close(self):
        pass
