from time import sleep, time
import argparse
import curses
import logging
import os
from threading import Thread
from abc import ABC, abstractmethod
from sila2.server import SilaServer
from sila2.client import ClientObservableCommandInstance
from sila2.framework import CommandExecutionStatus
from lara_simulation.simulated_serial import SimulatedDevice


class DeviceTest(ABC):
    server: SilaServer
    # something that also has a str() function representing the device status
    hardware_interface: SimulatedDevice

    def __init__(self):
        self.fast_mode = True
        self.show_mode = False
        self.kill_command = False
        self.last_command = None
        self.cmd_history = None
        self.feedback_history = None
        self.feedback = None
        self.generated_files = []
        self.error_flag = False

    # creates some nice string to describe the command and its response/error
    def feedbackToString(self, feedback):
        if feedback is None:
            return ["Result: Success\n"]
        elif self.error_flag:
            return [f"Result: {feedback.error_type}\n",
                    f"Description: {feedback.description}\n",
                    f"Details:{feedback.details}\n"
                    ]
        else:
            return [f"Result: Success\n",
                    f"Return: {str(feedback)}"]

    # creates and frequently updates the visual output of the simulated device
    def watch_server(self):
        window = curses.initscr()
        curses.start_color()
        curses.use_default_colors()
        curses.noecho()
        curses.cbreak()
        try:
            while not self.kill_command:
                window.clearok(True)
                s = str(self.hardware_interface)
                lines = s.split('\n')
                lines += [f"Last command: {self.last_command.__name__ if self.last_command else ''}\n"]
                lines += self.feedbackToString(self.feedback)
                lines += [f"Command before: {self.cmd_history.__name__ if self.cmd_history else ''}\n"]
                lines += self.feedbackToString(self.feedback_history)
                for i, line in enumerate(lines):
                    dim = window.getmaxyx()
                    if i < dim[0]-1 and len(line) <= dim[1]:
                        window.addstr(i, 0, line)
                window.refresh()
                curses.napms(500)
        finally:
            curses.echo()
            curses.nocbreak()
            curses.endwin()

    # makes the client send the specified command to the server. You can waiting times for both fast and real mode.
    def send_command(self, cmd, time_real=0.0, time_fast=0.0, args=[], to_fail=False, error_type=None):
        try:
            feedback = cmd(*args)
        except Exception as err:
            feedback = err
        self.feedback_history = self.feedback
        self.last_command = cmd
        self.feedback = feedback
        if self.fast_mode:
            sleep(time_fast)
        else:
            sleep(time_real)
        if to_fail:
            assert self.error_flag
            # todo check the exact type of error
        else:
            assert not self.error_flag

    def tearDown(self) -> None:
        self.kill_command = True
        try:
            self.server.stop()
            for f in self.generated_files:
                os.remove(f)
        except:
            pass

    def runSimulation(self):
        parsed_args = parse_command_line()
        start = time()
        logging.getLogger().setLevel(logging.ERROR)
        try:
            self.fast_mode = parsed_args.fast_mode
            self.show_mode = parsed_args.show_mode
            self.start_process(parsed_args.manual)
        finally:
            self.tearDown()
        print(f"test took {time() - start} seconds")

    def start_process(self, manual=False):
        if not self.fast_mode:
            self.hardware_interface.get_in_real_mode()
        if self.show_mode:
            t = Thread(name="Watchman", daemon=True, target=self.watch_server)
            t.start()
        sleep(.2) if self.fast_mode else sleep(1)
        if manual:
            self.manual_adjustments()
            while True:
                sleep(1)
        else:
            self.test_process()

        self.kill_command = True
        self.server.stop(grace_period=1)
        sleep(.2)
        print("Test finished successfully with all the anticipated errors :-)")

    @abstractmethod
    def test_process(self):
        raise NotImplementedError

    # Stuff that should happen in the simulation in case it is called for manual input
    def manual_adjustments(self):
        pass

    def finish_obs_cmd(self, cmd: ClientObservableCommandInstance, error_expected=False):
        while not cmd.done:
            sleep(.1)
        if error_expected:
            assert cmd.status == CommandExecutionStatus.finishedWithError
        else:
            if cmd.status == CommandExecutionStatus.finishedWithError:
                print(cmd.get_responses())
            assert cmd.status == CommandExecutionStatus.finishedSuccessfully

    def expect_error(self, cmd, args=[], error_type=None):
        try:
            cmd(*args)
            assert False
        except Exception as ex:
            if error_type is not None:
                assert type(ex) == error_type

    def wait(self, time_fast, time_real):
        if self.fast_mode:
            sleep(time_fast)
        else:
            sleep(time_real)


def parse_command_line():
    parser = argparse.ArgumentParser()
    # connection parameters
    parser.add_argument('-m', '--manual', action='store_true',
                        help='prevents the script from running the predefined '
                             'test_process. Instead, just a normal server is created.')
    parser.add_argument('-s', '--show-mode', action='store_true', help='creates a visualization (in ASCII to the '
                                                                       'standard output) of the device and the communication between server and client.')
    parser.add_argument('-r', '--real-time', action='store_false', dest='fast_mode', help='Makes the device react '
                                                                                          'and act (approximately) as slow as the real device would do.')
    parser.set_defaults(fast_mode=True, show_mode=False, manual=False)
    return parser.parse_args()
