import logging
from datetime import datetime
import os

_log_format = f"%(asctime)s - [%(levelname)s] - %(name)s - (%(filename)s).%(funcName)s(%(lineno)d) - %(message)s"
default_session_name = f"session_{datetime.today().ctime()}"
default_session_name = default_session_name.replace(" ", "_").replace(":", '|')


lvl_name = {
    logging.DEBUG: "DEBUG",
    logging.INFO: "INFO",
    logging.WARNING: "WARNING",
    logging.ERROR: "ERROR",
    logging.CRITICAL: "CRITICAL",
}


def get_file_handler(handler_name: str, lvl: int):
    file_handler = logging.FileHandler(handler_name)
    file_handler.setLevel(lvl)
    file_handler.setFormatter(logging.Formatter(_log_format))
    return file_handler


def get_stream_handler(lvl: int):
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(lvl)
    stream_handler.setFormatter(logging.Formatter(_log_format))
    return stream_handler


def configure_logging(session_name: str = default_session_name, stream_lvl: int = logging.WARNING):
    try:
        logger = logging.getLogger()
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "logs")
        for lvl, name in lvl_name.items():
            file_name = os.path.join(path, f"{session_name}_{name}.log")
            logger.addHandler(get_file_handler(file_name, lvl))
        logger.addHandler(get_stream_handler(stream_lvl))
        logging.getLogger().setLevel(logging.DEBUG)
    except OSError as err:
        print(f"Could not set uop logging ({err}). Are you using Windoof?. Logs will not be saved")



if __name__ == "__main__":
    configure_logging()
    logging.debug("Hello World")
    logging.info("the end is near")
    logging.error("Goodbye cruel world :-(")
