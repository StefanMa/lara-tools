from argparse import ArgumentParser
import time
import logging
import importlib
import traceback
import sys
import select
from arm_server.server import Server as ArmServer
from arm_server.generated.client import Client as ArmClient
from barcoderead_server.server import Server as BCRServer
from hotel_server.server import Server as HotelServer
from cytomat_server.server import Server as CytomatServer
from varioskan_server.server import Server as VarioskanServer
from rotanta_server.server import Server as RotantaServer
from bravo_server.server import Server as BravoServer
from lara_simulation.logging_manager import configure_logging
from sila2.client import SilaClient
from os import path


# hacky, but its quite useful to use fixed ports
ports_by_name = dict(
    arm=50051, centrifuge=50052, hotel=50053, cytomat=50054, cytomat2=50055, reader=50056, bc_reader=50057,
    bravo=50058, cytomat3=50059, cytomat4=50060, reader2=50061
)

lara_names = dict(
    arm="F5",
    centrifuge='Rotanta',
    hotel='Carousel',
    cytomat='Cytomat1550_1',
    cytomat2='Cytomat1550_2',
    cytomat3='Cytomat470',
    cytomat4='Cytomat2C',
    reader='VarioskanLUX',
    reader2='Varioskan',
    bc_reader='BarCodeReaderMS3',
    bravo='Bravo',
)


# can be used do
zoo_dir = path.abspath(path.dirname(__file__))


def cert_file_name(lara_name: str):
    return path.join(zoo_dir, f"{lara_name}_cert.pem")


server_type = dict(
    arm=ArmServer,
    centrifuge=RotantaServer,
    hotel=HotelServer,
    cytomat=CytomatServer,
    cytomat2=CytomatServer,
    cytomat3=CytomatServer,
    cytomat4=CytomatServer,
    reader=VarioskanServer,
    reader2=VarioskanServer,
    bc_reader=BCRServer,
    bravo=BravoServer,
)

server_args = dict(
    arm={},
    hotel={},
    reader=dict(server_name=lara_names['reader'], AIServer_address="http://localhost:8733/MIP/Reader?singleWsdl",
                event_receiver_port=8008),
    reader2=dict(server_name=lara_names['reader2'], AIServer_address="http://localhost:8734/MIP/Reader?singleWsdl",
                 event_receiver_port=8009),
    bravo={},
    cytomat=dict(serial_port='COM7', name="Cytomat1550_1"),
    cytomat2=dict(serial_port='COM8', name="Cytomat1550_2"),
    cytomat3=dict(serial_port='COM9', name="Cytomat470"),
    cytomat4=dict(serial_port='COM6', name="Cytomat2C"),
    bc_reader=dict(serial_port='COM17'),
    centrifuge=dict(serial_port='COM15'),
)

interacting = ['hotel', 'centrifuge', 'cytomat', 'cytomat2', 'cytomat3', 'cytomat4', 'reader', 'reader2', 'bravo']


def setup_interaction(interaction_devices=[], connect_barcode_reader=False, ip='127.0.0.1'):
    start = time.time()
    # connect barcode reader with arm
    arm_client = ArmClient.discover(server_name='F5', insecure=True)
    if connect_barcode_reader:
        arm_client.BarcodeReadingService.RegisterBarcodeReader(
            "BarCodeReaderMS3", ports_by_name['bc_reader'])
    # connect robot interaction clients
    for name in interaction_devices:
        print(f"connecting to {name}")
        arm_client.DeviceInteractionService.RegisterDevice(lara_names[name], ports_by_name[name])


class ZooKeeper:
    def __init__(self, devices=[], simulation=True, **kwargs):
        self.servers = {name: None for name in ports_by_name}
        try:
            for name in devices:
                device_kwargs = kwargs[name] if name in kwargs else {}
                print(f"starting server for {name} with args {device_kwargs}")
                self.servers[name] = server_type[name](simulation=simulation, **device_kwargs)
                self.servers[name].start_insecure('0.0.0.0', ports_by_name[name], enable_discovery=True)

            self.virtual_devices = {name: server.hardware_interface for name, server in self.servers.items()
                                    if server is not None}
            if simulation:
                self.reset(self.virtual_devices)
            print("setting up interaction")
            used = [d for d in interacting if d in devices]
            if 'arm' in devices:
                setup_interaction(interaction_devices=used,
                                  connect_barcode_reader='bc_reader' in devices)
            print("Servers are ready. Please press ctrl-c to stop")
            logging.getLogger().setLevel(logging.WARNING)
            while True:
                try:
                    if simulation:
                        self.print_stati(self.virtual_devices)
                    print("enter 'r' -> enter to reset all servers (only in simulation mode)", end='\r')
                    i, o, e = select.select([sys.stdin], [], [], .5)
                    if i:
                        entry = sys.stdin.readline().strip()
                        if entry == "r" and simulation:
                            print("\nresetting")
                            self.reset(self.virtual_devices)
                except Exception as ex:
                    print(ex)
                    print(traceback.print_exc())
                    print(f"Stop servers? [Y/Any_key]")
                    if input() in 'yY':
                        break

        finally:
            # stop the new servers
            for name in ports_by_name:
                if self.servers[name] is not None:
                    self.servers[name].stop()

    @staticmethod
    def print_stati(virtual_devices):
        for name, device in virtual_devices.items():
            with open(f"visual_{name}.txt", 'w') as outstream:
                port_string = f"port: {ports_by_name[name]}\n"
                outstream.write(port_string + str(device))

    @staticmethod
    def reset(virtual_devices):
        print(f"resetting in zookeeper: {virtual_devices}")
        for vd in virtual_devices.values():
            if 'inner_state' in vd.__dir__():
                vd.inner_state.reset()
        if 'hotel' in virtual_devices:
            for pos in range(16):
                virtual_devices['hotel'].add(0, pos)
            virtual_devices['hotel'].add(1, 0)
            virtual_devices['hotel'].turn(2, '')
        if 'cytomat4' in virtual_devices:
            virtual_devices['cytomat4'].inner_state.occupied[1][6] = True
            virtual_devices['cytomat4'].com.stack_occupancy[1][6] = True


def parse_args():
    parser = ArgumentParser(prog="zookeeper2", description="Run to start several SiLA 2servers")
    parser.add_argument("--lara", action="store_true", help="runs servers in real lara mode", default=False)

    return parser.parse_args()


if __name__ == '__main__':
    configure_logging(stream_lvl=logging.DEBUG)
    args = parse_args()
    zookeeper = ZooKeeper(
        devices=[
            'arm',
            'centrifuge',
            'reader',
            'reader2',
            'bc_reader',
            'cytomat',
            'cytomat2',
            #'cytomat3',
            'cytomat4',
            'hotel',
            'bravo',
        ],
        simulation=not args.lara,
        **server_args
    )
