import curses
import os

try:
    master_window = curses.initscr()
    x, y = master_window.getmaxyx()
    curses.noecho()
    curses.start_color()
    curses.use_default_colors()
    curses.cbreak()
    h = x
    w = y//2
    left_window = curses.newwin(x, w, 0, 0)
    right_window = curses.newwin(x, w, 0, w + 1)

    while True:
        to_visualize = sorted([filename for filename in os.listdir() if 'visual_' in filename])
        n = len(to_visualize)
        strings = []
        for filename in to_visualize:
            with open(filename, 'r') as instream:
                strings.append(instream.read())
        strings = sorted(strings, key=lambda s: len(s.split('\n')))
        left_strings = [strings[i] for i in range(n) if i % 2 == 0]
        right_strings = [strings[i] for i in range(n) if i % 2 == 1]
        left_lines = "\n".join(left_strings).split("\n")
        right_lines = "\n".join(right_strings).split("\n")
        left_window.clear()
        # fit into window
        left_lines = left_lines[:h]
        right_lines = right_lines[:h]
        for i, line in enumerate(left_lines):
            left_window.addstr(i, 0, line)
        left_window.refresh()
        right_window.clear()
        for i, line in enumerate(right_lines):
            right_window.addstr(i, 0, line)
        right_window.refresh()
        curses.napms(500)
finally:
    curses.echo()
    curses.nocbreak()
    curses.endwin()
