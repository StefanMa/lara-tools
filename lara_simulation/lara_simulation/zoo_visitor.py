import time
from lara_simulation.zookeeper import ports_by_name
from sila2.client import SilaClient
import traceback
import os
from lxml import etree
import plotly.express as ex
from plotly.figure_factory import create_distplot
from laborchestrator.structures import MoveStep, UsedDevice

from arm_server.generated.client import Client as ArmClient
from barcoderead_server.generated.client import Client as BCRClient
from varioskan_server.generated.client import Client as VarioskanClient
from cytomat_server.generated.client import Client as CytomatClient
from hotel_server.generated.client import Client as HotelClient
from sila2.client import ClientObservableCommandInstance

num_plates = 2
server_ip = "127.0.0.1"
method_name = "single_read"
shaking_time = 2
#server_ip = "141.53.22.61"
#method_name = "210924_varioskan_SPabs96_600_660_3"
#method_name = "211001_varioskan_single_well_600"
#shaking_time = 15


def init_clients(devices=[], ip=server_ip):
    clients = []
    for name in devices:
        try:
            client = SilaClient(ip, ports_by_name[name], insecure=True)
        except Exception as ex:
            print(f"creation of client for {name} went wrong: {ex}, {traceback.print_exc()}")
            client = None
        clients.append(client)
    return clients


def finish_observable_command(cmd, args):
    cmd_info = cmd(*args)
    time.sleep(.5)
    print(cmd_info)
    print(cmd_info.status)
    print(cmd_info.status.name)
    # 0,1 are waiting/running   2/3 are success/error
    while cmd_info.status.value < 2:
        end = '\r' if cmd_info.status.value < 2 else '\n'
        print(f"time_remaining:{cmd_info.estimated_remaining_time.seconds}"
              f": {cmd_info.status.name}", end=end)
        time.sleep(.1)
    print(cmd_info, cmd_info.get_responses())
    return cmd_info


def finish_with_error_timeout(cmd, args, timeout):
    start = time.time()
    while True:
        try:
            result = finish_observable_command(cmd, args)
            if result.status.value == 2:
                return result.get_responses()
        except:
            pass
        if time.time() - start > timeout:
            print("TIMEOUT")
            raise result.get_responses()
        else:
            time.sleep(1)


def observe_command(cmd: ClientObservableCommandInstance):
    while not cmd.done:
        if cmd.status is not None:
            print(f"status: {cmd.status.name}, time_remaining: {cmd.estimated_remaining_time}, progress:{cmd.progress}", end="\r")
        time.sleep(.1)
    print(f"\nstatus: {cmd.status.name}, response: {cmd.get_responses()} command took {cmd.lifetime_of_execution}")


def transfer_plates(arm: ArmClient, target: str, source: str, n: int, plate_type='lidded', source_offset=0, target_offset=0):
    for i in range(n):
        cmd = arm.RobotController.MovePlate((source, i+source_offset), (target, i+target_offset), plate_type)
        observe_command(cmd)
    print("Done :-)")

def reformat_animl_to_skanit6_xml(data_response):
    filepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "raw.xml")
    raw_xml = etree.parse(filepath)
    parent = raw_xml.find("//ResultStep")
    animl_xml = etree.fromstring(data_response.Data)
    # clear the namespaces
    for elem in animl_xml.getiterator():
        elem.tag = etree.QName(elem).localname
    content = animl_xml.find(".//Plate")
    parent.append(content)
    combined_bytes = etree.tostring(raw_xml, pretty_print=True)
    combined_string = str(combined_bytes, encoding='utf-8')
    return combined_string


def run_protocol():
    start = time.time()
    print('creating clients...')
    arm_client: ArmClient
    cytomat_client: CytomatClient
    reader_client: VarioskanClient
    arm_client, cytomat_client, reader_client = init_clients(
         devices=['arm', 'cytomat', 'reader']
    )
    print("starting process")

    cytomat_client.TemperatureController.ControlTemperature(273.15 + 37)
    # put plates into cytomat
    for i in range(num_plates):
        finish_observable_command(cmd=arm_client.RobotController.MovePlate,
                                  args=[("Carousel", i), ("Cytomat1550_1", i), 'unlidded'])
    # shake and incubate
    cytomat_client.ShakingController.SetShakingFrequency(700, 0)
    # wait until cytomat has finished loading
    finish_with_error_timeout(cmd=cytomat_client.ShakingController.StartShaking,
                              args=[0], timeout=60)
    # shake for 2 seconds
    time.sleep(shaking_time)

    # read the absorbance
    for i in range(num_plates-1):
        #reader_client.dataProvider_client.SetDataDir(r"")
        finish_observable_command(cmd=arm_client.RobotController.MovePlate,
                                  args=[("Cytomat1550_1", i), ("VarioskanLUX", 0), 'unlidded'])

        finish_with_error_timeout(cmd=reader_client.ProtocolExecutionService.ExecuteProtocol,
                                  args=[method_name], timeout=10)
        # store it in the second column
        finish_observable_command(cmd=arm_client.RobotController.MovePlate,
                                  args=[("VarioskanLUX", 0), ("Carousel", i+25), 'unlidded'])

    # put the other plate directly into carousel
    finish_observable_command(cmd=arm_client.RobotController.MovePlate,
                              args=[("Cytomat1550_1", 1), ("Carousel", 26), 'unlidded'])
    cytomat_client.ShakingController.StopShaking(-1)

    print(f"Protocol took {time.time()-start} seconds.")


if __name__ == '__main__':
    #run_protocol()
    pass
