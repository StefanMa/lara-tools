import curses
from lara_simulation.deviceTest import DeviceTest
from time import sleep


class LabVisualization:
    def __init__(self):
        self.first_port = 50051
        self._virtual_devices = []
        self._ports = []

    '''
    Adds the passed device to the list of devices to handle and automatically assigns a port
    '''
    def add_device(self, device: DeviceTest, port: int = -1):
        n = len(self._virtual_devices)
        self._virtual_devices.append(device)
        if port == -1:
            port = self.first_port + n
        self._ports.append(port)

    '''
    Here you can add custom commands to be executed after the normal setup of all devices
    '''
    def custom_setup(self):
        pass

    def run(self, visualize=True):
        n = len(self._virtual_devices)
        for device, port in zip(self._virtual_devices, self._ports):
            device.setUp(port=port)
            device.show_mode = True
            device.manual = True
        self.custom_setup()
        for device in self._virtual_devices:
            print(f"device {type(device)} is in fast mode? {device.fast_mode}")
        if visualize:
            try:
                curses.initscr()
                curses.noecho()
                curses.start_color()
                curses.use_default_colors()
                curses.cbreak()
                w = 60

                left_window = curses.newwin(43, w, 0, 0)
                right_window = curses.newwin(43, w, 0, w + 1)
                while True:
                    left_strings = [self._virtual_devices[i].server.hardware_interface.__str__() +
                                    f"\n server_port:{self._ports[i]}"
                                    for i in range(n) if i % 2 == 0]
                    right_strings = [self._virtual_devices[i].server.hardware_interface.__str__() +
                                     f"\n server_port:{self._ports[i]}"
                                     for i in range(n) if i % 2 == 1]
                    left_lines = "\n".join(left_strings).split("\n")
                    right_lines = "\n".join(right_strings).split("\n")
                    left_window.clear()
                    for i, line in enumerate(left_lines):
                        left_window.addstr(i, 0, line)
                    left_window.refresh()
                    right_window.clear()
                    for i, line in enumerate(right_lines):
                        right_window.addstr(i, 0, line)
                    right_window.refresh()
                    curses.napms(500)
            finally:
                for device in self._virtual_devices:
                    device.kill_command = True
                curses.echo()
                curses.nocbreak()
                curses.endwin()
        else:
            while True:
                for i, device in enumerate(self._virtual_devices):
                    with open(f"visual_{i}.txt", 'w') as outstream:
                        port_string = f"port: {device.server.server_port}\n"
                        outstream.write(port_string + str(device.server.hardware_interface))
                sleep(.5)

