from setuptools import find_packages, setup

setup(
    name="lara_utility",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
)