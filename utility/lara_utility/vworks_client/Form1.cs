﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SampleVWorksClient
{
    public partial class Form1 : Form
    {
        Queue<string> messages = new Queue<string>();
        public Form1()
        {
            VWorks_ = new VWorks4Lib.VWorks4APIClass();
            VWorks_.ProtocolAborted += new VWorks4Lib._IVWorks4APIEvents_ProtocolAbortedEventHandler(VWorks__ProtocolAborted);
            VWorks_.ProtocolComplete += new VWorks4Lib._IVWorks4APIEvents_ProtocolCompleteEventHandler(VWorks__ProtocolComplete);
            VWorks_.RecoverableError += new VWorks4Lib._IVWorks4APIEvents_RecoverableErrorEventHandler(VWorks__RecoverableError);
            VWorks_.UnrecoverableError += new VWorks4Lib._IVWorks4APIEvents_UnrecoverableErrorEventHandler(VWorks__UnrecoverableError);
            VWorks_.UserMessage += new VWorks4Lib._IVWorks4APIEvents_UserMessageEventHandler(VWorks__UserMessage);  
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            VWorks_ = new VWorks4Lib.VWorks4APIClass();
            //VWorks_.InitializationComplete += new VWorks4Lib._IVWorks4APIEvents_InitializationCompleteEventHandler(VWorks__InitializationComplete);
            logEventHandler = new VWorks4Lib._IVWorks4APIEvents_LogMessageEventHandler(VWorks__LogMessage);
            VWorks_.LogMessage += logEventHandler;
            //VWorks_.MessageBoxAction += new VWorks4Lib._IVWorks4APIEvents_MessageBoxActionEventHandler(VWorks__MessageBoxAction);
            VWorks_.ProtocolAborted += new VWorks4Lib._IVWorks4APIEvents_ProtocolAbortedEventHandler(VWorks__ProtocolAborted);
            VWorks_.ProtocolComplete += new VWorks4Lib._IVWorks4APIEvents_ProtocolCompleteEventHandler(VWorks__ProtocolComplete);
            VWorks_.RecoverableError += new VWorks4Lib._IVWorks4APIEvents_RecoverableErrorEventHandler(VWorks__RecoverableError);
            VWorks_.UnrecoverableError += new VWorks4Lib._IVWorks4APIEvents_UnrecoverableErrorEventHandler(VWorks__UnrecoverableError);
            VWorks_.UserMessage += new VWorks4Lib._IVWorks4APIEvents_UserMessageEventHandler(VWorks__UserMessage);

            VWorks_.Login("a", "");
            VWorks_.ShowVWorks(true);
        }

        void VWorks__UserMessage(int session, string caption, string message, bool wantsData, out string userData)
        {
            userData = "";
        }

        void VWorks__UnrecoverableError(int session, string description)
        {
            Console.WriteLine("Greetings from C#! Unrecoverable Error :-( " + description);
        }

        void VWorks__RecoverableError(int session, string device, string location, string description, out int actionToTake, out bool vworksHandlesError)
        {
            actionToTake = 2;
            vworksHandlesError = true;
            Console.WriteLine("Greetings from C#! Recoverable Error :-( " + description);
            messages.Append(String.Format("RecoverableError|{0}|{1}|{2}", session, device, description));
        }

        void VWorks__ProtocolComplete(int session, string protocol, string protocol_type)
        {
            Console.WriteLine("Greetings from C#! Protocol completed" + protocol + protocol_type);
            messages.Enqueue(String.Format("ProtocolComplete|{0}|{1}|{2}", session, protocol, protocol_type));
        }

        void VWorks__ProtocolAborted(int session, string protocol, string protocol_type)
        {
            Console.WriteLine("Greetings from C#! Protocol aborted " + protocol + protocol_type);
            messages.Append(String.Format("ProtocolAborted|{0}|{1}|{2}", session, protocol, protocol_type));
        }

        void VWorks__MessageBoxAction(int session, int type, string message, string caption, out int actionToTake)
        {
            Console.WriteLine("Greetings from C#! message box action " + type + caption + message);
            messages.Append(String.Format("ProtocolAborted|{0}|{1}|{2}", session, caption, messages));
            actionToTake = 1;
        }

        void VWorks__LogMessage(int session, int logClass, string timeStamp, string device, string location, string process, string task, string fileName, string message)
        {
            LogMessage_ = timeStamp + " " + message;
            //txtLog.Invoke(new MethodInvoker(AddLogEntry));
        }

        void VWorks__InitializationComplete(int session)
        {
            Console.WriteLine("Greetings from C#! Initialization complete  :-) ");
            messages.Append(String.Format("InitializationComplete|{0}", session));
        }

        public void AddLogEntry()
        {
            //txtLog.Text += LogMessage_ + "\r\n";
        }
        VWorks4Lib.VWorks4APIClass VWorks_;
        string LogMessage_;
        VWorks4Lib._IVWorks4APIEvents_LogMessageEventHandler logEventHandler;

        private void btnRunProtocol_Click(object sender, EventArgs e)
        {
            // VWorks fires the LogMessage event while loading the protocol, which
            // in this simple example causes things to lock, because this client can't 
            // respond to the event while waiting for RunProtocol to return, and RunProtocol
            // won't return unless the client responds to the event!  

            // Temporarily unsubscribe from log events
            VWorks_.LogMessage -= logEventHandler;

            // Start the protocol running
            //VWorks_.RunProtocol(txtProtocol.Text, 1);

            // Again subscribe to events
            VWorks_.LogMessage += logEventHandler;
        }
        void message(string s){
            Console.WriteLine("Greetings from C#! "+s);
        }
        int loggedReturn(VWorks4Lib.V11ReturnCode retcode, String method){
            message(String.Format("C# {0} returned {1}", method, retcode));
            return (int)retcode;
        }
        public int RunProtocol(string protocol, int runCount){
            return loggedReturn(VWorks_.RunProtocol(protocol, runCount), "RunProtocol");
        }
        public int AbortProtocol(){
            return loggedReturn(VWorks_.AbortProtocol(), "AbortProtocol");
        }
        public int CloseDeviceFile(string path){
            //return loggedReturn(VWorks_.CloseDeviceFile(path), "CloseDeviceFile");
            return 0;
        }
        public int CloseProtocol(string protocol){
            return loggedReturn(VWorks_.CloseProtocol(protocol), "CloseProtocol");
        }
        public int CompileProtocol(string protocol){
            int errors, warnings;
            VWorks4Lib.V11ReturnCode retcode;
            retcode = VWorks_.CompileProtocol(protocol, out errors, out warnings);
            return loggedReturn(retcode, "CompileProtocol");
        }
        public bool GetSimulationMode(){
            return VWorks_.GetSimulationMode();
        }
        public int GetTipStates(string protocol){       
	        string xml;
            VWorks4Lib.V11ReturnCode retcode = VWorks_.GetTipStates(protocol, out xml);
            return loggedReturn(retcode, String.Format("GetTipStates xml:{0}",xml));
        }
        public int LoadDeviceFile(string file){
            //return loggedReturn(VWorks_.LoadDeviceFile(file), "LoadDeviceFile");
            return 0;
        }

        public int Login(string user, string pw){
            VWorks4Lib.V11LoginResult result = VWorks_.Login(user, pw);
            Console.WriteLine(String.Format("Login-result: {0}", result));
            return (int)result;
        }
        public int Logout(){
            return loggedReturn(VWorks_.Logout(), "Logout");
        }
        public int ReinitializeDevices(){
            return loggedReturn(VWorks_.ReinitializeDevices(), "ReinitializeDevices");
        }
        public string GetMessages(){
            //Console.WriteLine(messages.Count());
            if (messages.Count() > 0){
                return messages.Dequeue();
            }
            return "";
        }

    }

    
}
