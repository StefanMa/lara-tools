import os
from simplejson import dumps
from threading import Thread
from time import sleep
import json
from copy import deepcopy


class InnerStatus:
    """
    In this class we handle the status data of any device which is not saved by the device itself.
    This data is saved to a json file frequently, so the server can be restarted without loosing this data.
    """
    def __init__(self, name, defaults, simulation_mode=True):
        self.filename = f"{name}_inner_status_{'sim' if simulation_mode else 'real'}.json"
        self.defaults = defaults
        for attr, value in defaults.items():
            self.__setattr__(attr, value)
        # load the last state
        if self.filename not in os.listdir():
            self.reset()
            self.save()
        else:
            self.load()
        self.auto_save_thread = Thread(target=self.auto_save, daemon=True)
        self.auto_save_thread.start()
        self.last_data = None
        self.runtime_backup()

    def copy_real_to_sim(self):
        self.save(self.filename.replace('real', 'sim'))

    def auto_save(self):
        while True:
            sleep(.2)
            if self.changes_happened():
                self.save()

    def changes_happened(self):
        for dat, dat_backup in zip([self.__getattribute__(attr) for attr in self.defaults.keys()],
                                   [self.last_data[attr] for attr in self.defaults.keys()]):
            if not dumps(dat) == dumps(dat_backup):
                return True
        return False

    def save(self, filename=None):
        if filename is None:
            filename = self.filename
        self.runtime_backup()
        with open(filename, "w") as outstream:
            json.dump(self.last_data, outstream, indent=4)

    def load(self, filename=None):
        if filename is None:
            filename = self.filename
        with open(filename, "r") as instream:
            data = json.load(instream)
            for attr in self.defaults.keys():
                self.__setattr__(attr, data[attr])
        self.runtime_backup()

    def runtime_backup(self):
        self.last_data = {attr: deepcopy(self.__getattribute__(attr)) for attr in self.defaults.keys()}

    def reset(self):
        for attr, value in self.defaults.items():
            self.__setattr__(attr, deepcopy(value))
