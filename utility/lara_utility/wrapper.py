import os
import traceback


class Wrapper:
    def Init(self, name): return "dummy_c_uuid"
    def HomeAll(self): return "dummy_c_uuid"
    def SmartMoveTo(self, position, offset): return "dummy_c_uuid"
    def SmartMoveSafe(self): return "dummy_c_uuid"
    def GripOpen(self): return "dummy_c_uuid"
    def GripClose(self, wrapper): return "dummy_c_uuid"
    def GripCheck(self, expectancy): return "dummy_c_uuid"
    def GripTo(self, distance): return "dummy_c_uuid"
    def GetGripperDistance(self): return 1
    def Get(self, position, offset, force): return "dummy_c_uuid"
    def Put(self, position, offset, force): return "dummy_c_uuid"
    def Delay(self, milliseconds): return "dummy_c_uuid"
    def MoveCartesian(self, dx, dy, dz): return "dummy_c_uuid"
    def MoveStraightTo(self, position): return "dummy_c_uuid"
    def GetMoverSpeed(self): return 1
    def GetMoverAcceleration(self): return 1
    def SetMoverSpeed(self, percentage): pass
    def SetMoverAcceleration(self, percentage): pass
    def Halt(self): pass
    def TurnOff(self): pass


def construct_wrapper(simulation, simulate_windoof=False):
    if simulate_windoof or not simulation:
        dll_file = "dummy_wrapper.dll" if simulation else 'real_wrapper.dll'
        # these are uncommon imports, so we scip them for users of only the simulation
        import clr
        import System
        try:
            p = os.path.join(os.path.join(os.path.dirname(__file__), 'cs_wrapper'), dll_file)
            dll = clr.AddReference(p)
            s = None
            for i in dll.DefinedTypes:
                s = str(i)
            cl = dll.GetType(s)
            wrapper = System.Activator.CreateInstance(cl)
            wrapper.DummyFunction()
            return wrapper
        except Exception as ex:
            print(f"Something went wrong constructing the Mover-C# wrapper :-(\n{ex}\n{traceback.print_exc()}")
            return None
    else:
        return Wrapper()


def construct_VWorks_wrapper():
    import clr
    import System
    try:
        p = os.path.join(os.path.join(os.path.dirname(__file__), 'vworks_client'), 'Form1.dll')
        dll = clr.AddReference(p)
        s = None
        for i in dll.DefinedTypes:
            s = str(i)
        cl = dll.GetType(s)
        wrapper = System.Activator.CreateInstance(cl)
        return wrapper
    except Exception as ex:
        print(f"Something went wrong constructing the VWorks-C# wrapper :-(\n{ex}\n{traceback.print_exc()}")
        return None